# Push Sender Config

Проект предоставляет приложение, использующее только QML.

Проект демонстрирует создание пакетов для установки файлов для других
приложений в вашей организации.

## Сборка проекта

Перед сборкой проекта нужно поместить yaml-файл в директорию [config](config).
Пример файла [конфигурации](config/app_server_configuration_example.yaml) расположен в этой же директории.

В остальном проект собирается обычным образом с помощью Аврора SDK.

Статус сборки:
1. example - [![pipeline status](https://gitlab.com/omprussia/examples/PushSenderConfig/badges/example/pipeline.svg)](https://gitlab.com/omprussia/examples/PushSenderConfig/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/examples/PushSenderConfig/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/examples/PushSenderConfig/-/commits/dev)  

## Использование

Поумолчанию путь для установки конфига /usr/share/common/ru.auroraos/PushSender/configuration.yaml.
Путь для конфига устанавливается в .spec файле.
В случаи необходимости путь можно изменить на /usr/share/common/ru.auroraos/PushSendeConfig/configuration.yaml.
Конкретный путь нужен, для приложения PushSender, чтобы загрузить конфигурационный файл.
Пример конфигурационного файла 'app_server_configuration_example.yaml' можно посмотреть в директории ./config

**Важно**
При установке конфигурационного файла в выбранную директорию, его нельзя будет перезаписать поверх по такому же пути 
установкой другого приложения, содержащего конфиг. Переустановить сам пакет PushSenderConfig с новым конфигом поверх старого можно.
1. Перед установкой PushSenderConfig на устройтство(эмулятор) нужно убедиться, что по нужному пути уже нету установленного
конфигурационного .yaml файла. Если его нет, что пакет ru.auroraos.PushSenderConfig установится, если есть переходим к шагу 2.
2. Есть два варианта. Первый - выбрать другой путь для установки, если он свободен. Таких путей может быть три:
/usr/share/common/\${ORGNAME}, /usr/share/common/\${ORGNAME}/\${APPNAME}, /usr/share/common/\${ORGNAME}/\${CONFAPPNAME}.
Второй - найти приложение, с которым был установлен конфигурационный файл. Только это приложение может обновлять конфигурационный файл.

Также нужно проверить работу push сервиса на устройстве. Его настройка описана в разделе [Особенности](#особенности).  

## Особенности

1. Версии Аврора ОС начиная с 5.0.1.  
Для корректной работы с Push-уведомлениями устройство должно быть зарегистрировано в Аврора Центр. Также для корректной работы необходимо, 
чтобы на устройстве был установлен актуальный для версии ОС клиент Аврора Центр. В этом случае конфиг для push сервиса заполнится автоматически. 

2.  Версии Аврора ОС до 5.0.1.  
Для корректной работы с уведомлениями необходимо настроить push сервис - задать adress, port и выставить флаг для crlValidation в false. 
Для получения текущих настроек, можно воспользоваться командой 
`devel-su gdbus call -y -d ru.omprussia.PushDaemon -o /ru/omprussia/PushDaemon -m ru.omprussia.PushDaemon.GetNetworkConfiguration`. 
Для установки параметров: `devel-su gdbus call -y -d ru.omprussia.PushDaemon -o /ru/omprussia/PushDaemon
-m ru.omprussia.PushDaemon.SetNetworkConfiguration "{'address':<'push-server.ru'>,
'port':<8000>,'crlValidation':<false>"}` (важно, чтобы хост был без протокола, например `https://`),
после чего необходимо перезапустить `push-daemon` с помощью команды
`devel-su systemctl restart push-daemon`.  

## Условия использования и участия

Исходный код проекта предоставляется по [лицензии](LICENSE.BSD-3-Clause.md),
которая позволяет использовать его в сторонних приложениях.

[Соглашение участника](CONTRIBUTING.md) регламентирует права,
предоставляемые участниками компании «Открытая Мобильная Платформа».

Информация об участниках указана в файле [AUTHORS](AUTHORS.md).

[Кодекс поведения](CODE_OF_CONDUCT.md) — это действующий набор правил
компании «Открытая Мобильная Платформа»,
который информирует об ожиданиях по взаимодействию между членами сообщества при общении и работе над проектами.


## Структура проекта

Проект имеет стандартную структуру приложения на базе C++ и QML для ОС Аврора.

Поскольку проект использует только QML, проект содержит только конфигурационный файл и файлы,
необходиые для QML.

* Файл **[ru.auroraos.PushSenderConfig.pro](ru.auroraos.PushSenderConfig.pro)** описывает структуру проекта для системы сборки qmake.
* Каталог **[config](config)** содержит .yaml файл конфигурации.
* Каталог **[icons](icons)** содержит иконки приложения для поддерживаемых разрешений экрана.
* Каталог **[qml](qml)** содержит исходный код на QML и ресурсы интерфейса пользователя.
  * Файл **[ru.auroraos.PushSenderConfig.qml](qml/ru.auroraos.PushSenderConfig.qml)** предоставляет реализацию окна приложения.
* Каталог **[rpm](rpm)** содержит настройки сборки rpm-пакета.
  * Файл **[ru.auroraos.PushSenderConfig.spec](rpm/ru.auroraos.PushSenderConfig.spec)** используется инструментом rpmbuild.
* Файл **[ru.auroraos.PushSenderConfig.desktop](ru.auroraos.PushSenderConfig.desktop)** определяет отображение и параметры запуска приложения.

## This document in English

- [README.md](README.md)
