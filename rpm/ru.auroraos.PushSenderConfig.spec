Name:      ru.auroraos.PushSenderConfig
Summary:   Organization shared files
Version:   0.1
Release:   1
License:   BSD-3-Clause
BuildArch: noarch
URL:       https://developer.auroraos.ru/open-source
Source0:   %{name}-%{version}.tar.bz2

# Aurora SDK version
%define aurora_sdk_version %(cat /etc/os-release | grep VERSION_ID | cut -d '=' -f2)
# Parsing aurora_sdk_version by number
# 1. major version sdk
%define aurora_sdk_major_version %(echo %{aurora_sdk_version} | cut -f1 -d".")
# 2. minor version sdk
%define aurora_sdk_minor_version %(echo %{aurora_sdk_version} | cut -f2 -d".")
# 3. build version sdk
%define aurora_sdk_build_version %(echo %{aurora_sdk_version} | cut -f3 -d".")
# 4. revision version sdk
%define aurora_sdk_revision_version %(echo %{aurora_sdk_version} | cut -f4 -d".")

# Path to install the config file
%define install_path_config %{_datadir}/common/ru.auroraos/PushSender
# If you need to install the configuration file to PushSenderConfig directory please
# change %%{_datadir}/common/ru.auroraos/PushSender to %%{_datadir}/common/ru.auroraos.

BuildRequires: pkgconfig(Qt5Quick)
BuildRequires: pkgconfig(Qt5Qml)
Requires: libauroraapp-launcher
Requires: sailfishsilica-qt5 >= 0.10.9

%description
The project provides a QML-only applications.

%prep
%autosetup

%build

%qmake5 \
AURORA_SDK_VERSION=%{aurora_sdk_version} \
AURORA_SDK_MAJOR_VERSION=%{aurora_sdk_major_version} \
AURORA_SDK_MINOR_VERSION=%{aurora_sdk_minor_version} \
AURORA_SDK_BUILD_VERSION=%{aurora_sdk_build_version} \
AURORA_SDK_REVISION_VERSION=%{aurora_sdk_revision_version} \
INSTALL_PATH_CONFIG=%{install_path_config}

%make_build

%install
%make_install

%files
%{install_path_config}/configuration.yaml
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
