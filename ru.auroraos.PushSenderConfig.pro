# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.PushSenderConfig

lessThan(AURORA_SDK_VERSION, 4.0.2.174) {
    CONFIG += sailfishapp_qml
} else {
    CONFIG += auroraapp_qml
}

DISTFILES += \
    config/*.yaml \
    qml/ru.auroraos.PushSenderConfig.qml \
    rpm/ru.auroraos.PushSenderConfig.spec \
    ru.auroraos.PushSenderConfig.desktop \
    AUTHORS.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    LICENSE.BSD-3-Clause.md \
    README.md \
    README.ru.md

config_install.extra = cp $${PWD}/config/*.yaml $${OUT_PWD}/configuration.yaml
config_install.files = $${OUT_PWD}/configuration.yaml
config_install.path = $${INSTALL_PATH_CONFIG}
config_install.CONFIG = no_check_exist

INSTALLS += \
    config_install \

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172
