# Push Sender Config

The project provides a QML-only applications.

This project demonstrates how you can create packages to install files 
for other applications in your organization.

## Building

Before building the project, you need to put the yaml file in the [config](config) directory.
An example of a [configuration](config/app_server_configuration_example.yaml) file is located
in the same directory.

Otherwise, the project is built in the usual way using the Aurora SDK.

Build status:
1. example - [![pipeline status](https://gitlab.com/omprussia/examples/PushSenderConfig/badges/example/pipeline.svg)](https://gitlab.com/omprussia/examples/PushSenderConfig/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/examples/PushSenderConfig/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/examples/PushSenderConfig/-/commits/dev)  

## Usage

The default path for config installation is /usr/share/common/en.auroraos/PushSender/configuration.yaml.
The path for the config is set in the .spec file.
If necessary, the path can be changed to /usr/share/common/en.auroraos/PushSendeConfig/configuration.yaml.
The specific path is needed for the PushSender application to load the configuration file.
An example configuration file 'app_server_configuration_example.yaml' can be seen in the ./config directory

**Important**.
If you install a config file in the selected directory, it cannot be overwritten over the same path 
by installing another application containing the config. You can reinstall the PushSenderConfig package itself with the new config over the old one.
1. Before installing PushSenderConfig on the device (emulator), make sure that there is no already installed configuration .yaml file on the required path.
configuration .yaml file. If there is no such file, the package ru.auroraos.PushSenderConfig will be installed, if there is, go to step 2.
1. There are two options. The first one is to choose another path for installation, if it is free. There can be three such paths:
/usr/share/common/\${ORGNAME}, /usr/share/common/\${ORGNAME}/\${APPNAME}, /usr/share/common/\${ORGNAME}/\${CONFAPPNAME}.
The second is to find the application with which the configuration file was installed. Only that application can update the configuration file.  

You should also check if the push service works on your device. Its configuration is described in [Features](#features).  

## Features

1. Aurora OS versions starting from 5.0.1.  
For correct operation with Push notifications, the device must be registered in Aurora Center. Also for correct operation it is necessary, 
Aurora Center client, which is up-to-date for the OS version, must be installed on the device. In this case the config for push service will be filled automatically. 

2.  Aurora OS versions up to 5.0.1.  
For correct work with notifications it is necessary to configure the push service - set adress, port and set the flag for crlValidation to false. 
To get the current settings, you can use the command 
`devel-su gdbus call -y -d ru.omprussia.PushDaemon -o /ru/omprussia/PushDaemon -m ru.omprussia.PushDaemon.GetNetworkConfiguration`. 
To set the parameters: `devel-su gdbus call -y -d ru.omprussia.PushDaemon -o /ru/omprussia/PushDaemon
-m ru.omprussia.PushDaemon.SetNetworkConfiguration “{‘address’:<‘push-server.ru’>,
'port':<8000>,'crlValidation':<false>"}` (it is important that the host is protocol-less, for example `https://`),
after which `push-daemon` must be restarted using the command
`devel-su systemctl restart push-daemon`.  

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.


## Project Structure

The project has a standard structure
of an application based on C++ and QML for Aurora OS.

Since the project uses only QML, the project contains only the configuration
file and files, necessary for QML.

* **[ru.auroraos.PushSenderConfig.pro](ru.auroraos.PushSenderConfig.pro)** file describes the project structure for the qmake build system.
* **[config](config)** directory contains .yaml file configuration.
* **[icons](icons)** directory contains application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[ru.auroraos.PushSenderConfig.qml](qml/ru.auroraos.PushSenderConfig.qml)** file provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.PushSenderConfig.spec](rpm/ru.auroraos.PushSenderConfig.spec)** file is used by rpmbuild tool.
* **[ru.auroraos.PushSenderConfig.desktop](ru.auroraos.PushSenderConfig.desktop)** file defines the display and parameters for launching the application.

## Compatibility
  
The project is compatible with all the current versions of the Aurora OS.

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
